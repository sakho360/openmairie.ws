import logging
import requests

from bs4 import BeautifulSoup

log = logging.getLogger(__name__)

# XXX store conf in DB
CONF = {
    'login': 'admin',
    'password': 'admin',
    'url': 'http://localhost/openads',
    'instance': 'openads'
}


class OMBaseObject(object):
    """
    """

    fields_mapping = {}

    is_authenticated = False
    cookies = None

    url = instance = login = password = None

    def __init__(self, **args):
        """ """
        self._load_conf()

    # XXX data should be get from DB
    def _load_conf(self):
        """ """
        self.url = CONF['url']
        self.instance = CONF['instance']
        self.login = CONF['login']
        self.password = CONF['password']

    def authenticate(self):
        """ """
        if self.is_authenticated:
            return True

        hello = requests.get(self.url)
        self.cookies = {
            self.instance: hello.cookies[self.instance]
        }

        url = self.url + '/app/index.php?module=login'
        data = {
            'login': self.login,
            'password': self.password,
            'login.action.connect': 'Se connecter'
        }
        response = requests.post(url, data=data, cookies=self.cookies)
        if response.status_code != 200:
            return

        soup = BeautifulSoup(response.content, 'html.parser')
        if not soup.select("#header #actions a.actions-logout"):
            return

        self.is_authenticated = True

        return True

    def _get(self, resource, id):
        """ """
        if not self.authenticate():
            raise RuntimeError

        url = self.url + '/app/index.php?module=form&obj=%s&action=3&idx=%s' % (resource, id)
        log.debug('GET %s' % url)
        response = requests.get(url, cookies=self.cookies)
        if response.status_code != 200:
            # XXX do something useful
            return

        return BeautifulSoup(response.content, 'html.parser')

    def _set(self, resource, data):
        """ """
        if not self.authenticate():
            raise RuntimeError

        url = self.url + '/app/index.php?module=form&obj=%s&validation=1&action=0&retour=form' % resource
        log.debug('POST %s' % url)
        response = requests.post(url, data=data, cookies=self.cookies)
        if response.status_code != 200:
            # XXX do something useful 
            return

        return BeautifulSoup(response.content, 'html.parser')

    def get_fields(self):
        """ """
        return self.fields_mapping.keys()
