from setuptools import setup, find_packages

setup(
    name="openmairie.ws",
    version="0.1.0",
    url="https://gitlab.com/atreal/openmairie.ws",

    author="atReal",
    author_email="contact@atreal.fr",

    description="Base resource for openMairie WS",
    long_description=open('README.rst').read(),

    packages=find_packages("src"),
    package_dir={"": "src"},
    namespace_packages=["openmairie"],


    install_requires=['requests', 'bs4'],

    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
)
