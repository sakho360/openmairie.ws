openmairie.ws
=============

.. image:: https://img.shields.io/pypi/v/openmairie.ws.svg
    :target: https://pypi.python.org/pypi/openmairie.ws
    :alt: Latest PyPI version

.. image:: https://travis-ci.org/atreal/openmairie.ws.png
   :target: https://travis-ci.org/atreal/openmairie.ws
   :alt: Latest Travis CI build status

Base ressources for openMairie WebSer

Usage
-----

Installation
------------

Requirements
^^^^^^^^^^^^

Compatibility
-------------

Licence
-------

Authors
-------

`openmairie.ws` was written by `atReal <contact@atreal.fr>`_.
